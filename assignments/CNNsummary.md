## Convolutional Neural Networkn (CNN) ##

A Convolutional Neural Network is a deep learning algorithm which can take in an input image, assign importanc to various aspects/objects in the image and be able to differentiate one from the other. 

_Eg: we create a filter (matrix) which has a particular feature e.g. one filter with eye, one filter with dog ears etc._

[**Basic Overview of working of CNN**](https://gitlab.com/iotiotdotin/ai-user-training/-/wikis/uploads/8280d7f007a9f7d07eae922ea7102477/dnn2.png)  
**Important Terms**
1. **Kernel size (K):** Refers to the width x height of the filter.  
![](https://gitlab.com/iotiotdotin/ai-user-training/-/wikis/uploads/3db88c10d6907797e7e1e50c2d5a1532/dnn3.png)
2. **Stride (S):** It is the number of pixels shifts over the input matrix.
3. **Zero padding (pad):** The amount of zeros to put on the image border. Using padding allows the kernel to completely filter every location of an input image, including the edges.  
![](https://gitlab.com/iotiotdotin/ai-user-training/-/wikis/uploads/8a686527a5bcddf104679705e4a3ebc7/dnn4.png)
4. **Number of filters (F):** How many filters our convolution layer will have. It controls the number of patterns or features that a convolution layer will look for.
5. **Flattening :** Flattening layer is used to bring all feature maps matrices into a single vector.  [Click Here for reference](https://www.youtube.com/watch?v=mFAIBMbACMA&feature=youtu.be)

