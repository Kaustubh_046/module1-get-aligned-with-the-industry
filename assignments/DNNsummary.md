## Deep Neural Network ##
### What is Neural Network? ###
An artificial neural network learning algorithm, or neural network, or just neural net, is a computational learning system that uses a network of functions to understand and translate a data input of one form into a desired output, usually in another form. The concept of the artificial neural network was inspired by human biology and the way neurons of the human brain function together to understand inputs from human senses. 

* Neoron is a thing that hold a number between 0 and 1. This number is called activation.  

![](https://www.cs.swarthmore.edu/~meeden/cs63/s19/labs/nn.png)  
* Activation in one layer activate the other layers. 
 ### Layers of NN ###
 **Input Layer:** An image which an array of pixels is the primary layer of a NN. The pixels have activation value in grayscale.  
 **Hidden Layers:**  The hidden layers perform nonlinear transformations of the inputs entered into the network.  
 **Output Layer:**  This layer is the last layer in the network & receives input from the last hidden layer. With this layer we can get desired number of values and in a desired range. 

### Cost Function ###  
The loss function computes the error for a single training example. The cost function is the average of the loss functions of the entire training set.  
### Gradient Descent ###
Gradient descent is an optimization algorithm used to find the values of parameters (coefficients) of a function (f) that minimizes a cost function (cost).
![](https://miro.medium.com/max/2284/1*jNyE54fTVOH1203IwYeNEg.png)  

Gradient descent is best used when the parameters cannot be calculated analytically (e.g. using linear algebra) and must be searched for by an optimization algorithm.
### BackPropagation ###
For example, let's consider we need to output two neuron to be activated for an input image of 2, and the activation inside it is 0.2 and the neuron 2 is most likely not going to get activated unless its activation is almost near to one.
This can be done by-
1. Increasing the bias   
2. Increasing weights 
3. Change activation of previous layers  
